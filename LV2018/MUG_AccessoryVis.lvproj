﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="controls" Type="Folder">
			<Item Name="Base Height Distance.ctl" Type="VI" URL="../controls/Base Height Distance.ctl"/>
			<Item Name="Base ROI Height.ctl" Type="VI" URL="../controls/Base ROI Height.ctl"/>
			<Item Name="Base Width Distance.ctl" Type="VI" URL="../controls/Base Width Distance.ctl"/>
			<Item Name="Carton Geometry.ctl" Type="VI" URL="../controls/Carton Geometry.ctl"/>
			<Item Name="Carton Point Data.ctl" Type="VI" URL="../controls/Carton Point Data.ctl"/>
			<Item Name="Color Threshold.ctl" Type="VI" URL="../controls/Color Threshold.ctl"/>
			<Item Name="Defect Class.ctl" Type="VI" URL="../controls/Defect Class.ctl"/>
			<Item Name="Defect Data.ctl" Type="VI" URL="../controls/Defect Data.ctl"/>
			<Item Name="Defect position Reference.ctl" Type="VI" URL="../controls/Defect position Reference.ctl"/>
			<Item Name="defectType.ctl" Type="VI" URL="../controls/defectType.ctl"/>
			<Item Name="File Types.ctl" Type="VI" URL="../controls/File Types.ctl"/>
			<Item Name="Flap Type.ctl" Type="VI" URL="../controls/Flap Type.ctl"/>
			<Item Name="FullColorThresholdData.ctl" Type="VI" URL="../controls/FullColorThresholdData.ctl"/>
			<Item Name="Horizontal Placement.ctl" Type="VI" URL="../controls/Horizontal Placement.ctl"/>
			<Item Name="pointWithAngle.ctl" Type="VI" URL="../controls/pointWithAngle.ctl"/>
			<Item Name="ROI Height Selector.ctl" Type="VI" URL="../controls/ROI Height Selector.ctl"/>
			<Item Name="ROI size.ctl" Type="VI" URL="../controls/ROI size.ctl"/>
			<Item Name="ROI Width Selector.ctl" Type="VI" URL="../controls/ROI Width Selector.ctl"/>
			<Item Name="ROIInfo.ctl" Type="VI" URL="../controls/ROIInfo.ctl"/>
			<Item Name="ROIRatios.ctl" Type="VI" URL="../controls/ROIRatios.ctl"/>
			<Item Name="ROIRatiosAll.ctl" Type="VI" URL="../controls/ROIRatiosAll.ctl"/>
			<Item Name="ROITranslations.ctl" Type="VI" URL="../controls/ROITranslations.ctl"/>
			<Item Name="setup.ctl" Type="VI" URL="../controls/setup.ctl"/>
			<Item Name="threshold data.ctl" Type="VI" URL="../controls/threshold data.ctl"/>
			<Item Name="Vertical Placement.ctl" Type="VI" URL="../controls/Vertical Placement.ctl"/>
		</Item>
		<Item Name="Deprecated" Type="Folder">
			<Item Name="DEPRECATED_GenerateROIFromTranslations.vi" Type="VI" URL="../subvis/DEPRECATED_GenerateROIFromTranslations.vi"/>
			<Item Name="DEPRECATED_GenThresholdDataManual.vi" Type="VI" URL="../subvis/DEPRECATED_GenThresholdDataManual.vi"/>
			<Item Name="profileRejectZero.vi" Type="VI" URL="../subvis/profileRejectZero.vi"/>
		</Item>
		<Item Name="Geometry" Type="Folder">
			<Item Name="CalPosition.vi" Type="VI" URL="../subvis/CalPosition.vi"/>
		</Item>
		<Item Name="Overlay" Type="Folder">
			<Item Name="ColorSelector.vi" Type="VI" URL="../subvis/ColorSelector.vi"/>
			<Item Name="GenerateObjectOverlay.vi" Type="VI" URL="../subvis/GenerateObjectOverlay.vi"/>
			<Item Name="OverlayDefectData.vi" Type="VI" URL="../subvis/OverlayDefectData.vi"/>
			<Item Name="OverlayPointDataArray.vi" Type="VI" URL="../subvis/OverlayPointDataArray.vi"/>
		</Item>
		<Item Name="ROI" Type="Folder">
			<Item Name="!Manager_ROI.vi" Type="VI" URL="../subvis/!Manager_ROI.vi"/>
			<Item Name="Generate Object ROI.vi" Type="VI" URL="../subvis/Generate Object ROI.vi"/>
			<Item Name="Generate ROI from point data.vi" Type="VI" URL="../subvis/Generate ROI from point data.vi"/>
			<Item Name="Generate ROI size.vi" Type="VI" URL="../subvis/Generate ROI size.vi"/>
			<Item Name="Generate ROI Translations.vi" Type="VI" URL="../subvis/Generate ROI Translations.vi"/>
			<Item Name="GenerateAndTranslateROI.vi" Type="VI" URL="../GenerateAndTranslateROI.vi"/>
			<Item Name="GenerateBaseReference.vi" Type="VI" URL="../subvis/GenerateBaseReference.vi"/>
			<Item Name="GenerateRectangleBoundingPoints.vi" Type="VI" URL="../subvis/GenerateRectangleBoundingPoints.vi"/>
			<Item Name="GetMidpoint.vi" Type="VI" URL="../subvis/GetMidpoint.vi"/>
			<Item Name="HistogramThresholdandWeight.vi" Type="VI" URL="../subvis/HistogramThresholdandWeight.vi"/>
			<Item Name="RepositionAnchor.vi" Type="VI" URL="../subvis/RepositionAnchor.vi"/>
			<Item Name="Translate Individual ROI points.vi" Type="VI" URL="../subvis/Translate Individual ROI points.vi"/>
			<Item Name="TranslateROIAngle.vi" Type="VI" URL="../subvis/TranslateROIAngle.vi"/>
			<Item Name="UpdatePointDataAngle.vi" Type="VI" URL="../subvis/UpdatePointDataAngle.vi"/>
			<Item Name="ValidateROIRatios.vi" Type="VI" URL="../subvis/ValidateROIRatios.vi"/>
		</Item>
		<Item Name="subVIs" Type="Folder">
			<Item Name="ChooseMajorMinor.vi" Type="VI" URL="../subvis/ChooseMajorMinor.vi"/>
			<Item Name="Defect Handler.vi" Type="VI" URL="../subvis/Defect Handler.vi"/>
			<Item Name="ErrorCartonGeometryHandler.vi" Type="VI" URL="../subvis/ErrorCartonGeometryHandler.vi"/>
			<Item Name="GenerateThresholdData.vi" Type="VI" URL="../subvis/GenerateThresholdData.vi"/>
			<Item Name="Get ASCII string.vi" Type="VI" URL="../subvis/Get ASCII string.vi"/>
			<Item Name="getFilenames.vi" Type="VI" URL="../subvis/getFilenames.vi"/>
			<Item Name="HandlerBarcode.vi" Type="VI" URL="../subvis/HandlerBarcode.vi"/>
			<Item Name="IMAQ_CreateAndRename.vi" Type="VI" URL="../subvis/IMAQ_CreateAndRename.vi"/>
			<Item Name="IMAQ_MultithresholdWrapper.vi" Type="VI" URL="../subvis/IMAQ_MultithresholdWrapper.vi"/>
			<Item Name="ResizeImageArray.vi" Type="VI" URL="../subvis/ResizeImageArray.vi"/>
		</Item>
		<Item Name="Utilities" Type="Folder">
			<Item Name="Manage Defect Dataset.vi" Type="VI" URL="../subvis/Manage Defect Dataset.vi"/>
			<Item Name="ManageColorThresholdJSON.vi" Type="VI" URL="../subvis/ManageColorThresholdJSON.vi"/>
			<Item Name="ManageColorThresholdXML.vi" Type="VI" URL="../subvis/ManageColorThresholdXML.vi"/>
			<Item Name="ManageDefectXML.vi" Type="VI" URL="../ManageDefectXML.vi"/>
			<Item Name="TestROI.vi" Type="VI" URL="../TestROI.vi"/>
			<Item Name="writeSetupJSON.vi" Type="VI" URL="../writeSetupJSON.vi"/>
		</Item>
		<Item Name="VBAI" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="VBAI_ClosestCarton.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_ClosestCarton.vi"/>
			<Item Name="VBAI_ComputeColorResults.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_ComputeColorResults.vi"/>
			<Item Name="VBAI_DefectPLCOutput.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_DefectPLCOutput.vi"/>
			<Item Name="VBAI_Determine Carton Number.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_Determine Carton Number.vi"/>
			<Item Name="VBAI_ProcessROIs.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_ProcessROIs.vi"/>
			<Item Name="VBAI_updateDefectCount.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_updateDefectCount.vi"/>
			<Item Name="VBAI_ExtractColorPlanes.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_ExtractColorPlanes.vi"/>
			<Item Name="VBAI_UpdateResultArray.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_UpdateResultArray.vi"/>
			<Item Name="VBAI_thresholdAndExtract.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_thresholdAndExtract.vi"/>
			<Item Name="VBAI_AngleTranslate.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_AngleTranslate.vi"/>
			<Item Name="VBAI_StripPath.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_StripPath.vi"/>
			<Item Name="VBAI_ExtractImageFromBoundingPoints.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_ExtractImageFromBoundingPoints.vi"/>
			<Item Name="VBAI_CalculateMaximalThreshold.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_CalculateMaximalThreshold.vi"/>
			<Item Name="VBAI_MeasureMaximalIntensity.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_MeasureMaximalIntensity.vi"/>
			<Item Name="VBAI_HistogramWrapper.vi" Type="VI" URL="../VBAI_HistogramWrapper.vi"/>
			<Item Name="VBAI_BarcodeWrapper.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_BarcodeWrapper.vi"/>
			<Item Name="VBAI_GenerateTimeStamp.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_GenerateTimeStamp.vi"/>
			<Item Name="VBAI_GetConfigFilenames.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_GetConfigFilenames.vi"/>
			<Item Name="VBAI_!TestPoint.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_!TestPoint.vi"/>
			<Item Name="VBAI_AddClassifierSample.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_AddClassifierSample.vi"/>
			<Item Name="VBAI_profileExtractCarton.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_profileExtractCarton.vi"/>
			<Item Name="VBAI_GenerateROIAndThresholds.vi" Type="VI" URL="../VbaiRunLabViewVIs/VBAI_GenerateROIAndThresholds.vi"/>
		</Item>
		<Item Name="!InspectionInterface.vi" Type="VI" URL="../!InspectionInterface.vi"/>
		<Item Name="BuildToDestination.vi" Type="VI" URL="../BuildToDestination.vi"/>
		<Item Name="GenerateFiltered16BitPercentiles.vi" Type="VI" URL="../subvis/GenerateFiltered16BitPercentiles.vi"/>
		<Item Name="imageFilter16bit0.vi" Type="VI" URL="../imageFilter16bit0.vi"/>
		<Item Name="ValidatePercentile.vi" Type="VI" URL="../subvis/ValidatePercentile.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names and Paths Arrays - path__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names and Paths Arrays__ogtk.vi"/>
				<Item Name="Build Path - File Names Array - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names Array - path__ogtk.vi"/>
				<Item Name="Build Path - File Names Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names Array__ogtk.vi"/>
				<Item Name="Build Path - Traditional - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - Traditional - path__ogtk.vi"/>
				<Item Name="Build Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - Traditional__ogtk.vi"/>
				<Item Name="Build Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path__ogtk.vi"/>
				<Item Name="Create Dir if Non-Existant__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Create Dir if Non-Existant__ogtk.vi"/>
				<Item Name="File Exists - Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists - Array__ogtk.vi"/>
				<Item Name="File Exists - Scalar__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists - Scalar__ogtk.vi"/>
				<Item Name="File Exists__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists__ogtk.vi"/>
				<Item Name="List Directory__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/List Directory__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array__ogtk.vi"/>
				<Item Name="Strip Path - Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Arrays__ogtk.vi"/>
				<Item Name="Strip Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Traditional__ogtk.vi"/>
				<Item Name="Strip Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path__ogtk.vi"/>
				<Item Name="Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Barcode Search Options.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Instrument.llb/Barcode Search Options.ctl"/>
				<Item Name="Cast Image 2 Method.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Management.llb/Cast Image 2 Method.ctl"/>
				<Item Name="Color (U64)" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Color (U64)"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="Image Type" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Type"/>
				<Item Name="Image Unit" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Unit"/>
				<Item Name="IMAQ Cast Image 2" Type="VI" URL="/&lt;vilib&gt;/vision/Management.llb/IMAQ Cast Image 2"/>
				<Item Name="IMAQ Classifier Engine Type.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Classification.llb/IMAQ Classifier Engine Type.ctl"/>
				<Item Name="IMAQ Classifier Read Options.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Classification.llb/IMAQ Classifier Read Options.ctl"/>
				<Item Name="IMAQ Classifier Session.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Classification.llb/IMAQ Classifier Session.ctl"/>
				<Item Name="IMAQ Classifier Type.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Classification.llb/IMAQ Classifier Type.ctl"/>
				<Item Name="IMAQ Classifier Write Options.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Classification.llb/IMAQ Classifier Write Options.ctl"/>
				<Item Name="IMAQ Convert Point to ROI" Type="VI" URL="/&lt;vilib&gt;/vision/ROI Conversion.llb/IMAQ Convert Point to ROI"/>
				<Item Name="IMAQ Convert Rectangle to ROI" Type="VI" URL="/&lt;vilib&gt;/vision/ROI Conversion.llb/IMAQ Convert Rectangle to ROI"/>
				<Item Name="IMAQ Convert Rectangle to ROI (Polygon)" Type="VI" URL="/&lt;vilib&gt;/vision/ROI Conversion.llb/IMAQ Convert Rectangle to ROI (Polygon)"/>
				<Item Name="IMAQ Convert ROI to Point" Type="VI" URL="/&lt;vilib&gt;/vision/ROI Conversion.llb/IMAQ Convert ROI to Point"/>
				<Item Name="IMAQ Convert ROI to Rectangle" Type="VI" URL="/&lt;vilib&gt;/vision/ROI Conversion.llb/IMAQ Convert ROI to Rectangle"/>
				<Item Name="IMAQ Coordinate System" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Coordinate System"/>
				<Item Name="IMAQ Copy" Type="VI" URL="/&lt;vilib&gt;/vision/Management.llb/IMAQ Copy"/>
				<Item Name="IMAQ Count Objects 2" Type="VI" URL="/&lt;vilib&gt;/vision/Blob.llb/IMAQ Count Objects 2"/>
				<Item Name="IMAQ Create" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Create"/>
				<Item Name="IMAQ Dispose" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Dispose"/>
				<Item Name="IMAQ Dispose Classifier" Type="VI" URL="/&lt;vilib&gt;/vision/Classification.llb/IMAQ Dispose Classifier"/>
				<Item Name="IMAQ GetImageInfo" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ GetImageInfo"/>
				<Item Name="IMAQ GetImageSize" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ GetImageSize"/>
				<Item Name="IMAQ Image.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Image.ctl"/>
				<Item Name="IMAQ ImageToArray" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ ImageToArray"/>
				<Item Name="IMAQ Overlay Line" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Line"/>
				<Item Name="IMAQ Overlay Oval" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Oval"/>
				<Item Name="IMAQ Overlay Points" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Points"/>
				<Item Name="IMAQ Overlay Rectangle" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Rectangle"/>
				<Item Name="IMAQ Overlay ROI" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay ROI"/>
				<Item Name="IMAQ Overlay Text" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Text"/>
				<Item Name="IMAQ Particle Filter 2" Type="VI" URL="/&lt;vilib&gt;/vision/Compatibility.llb/IMAQ Particle Filter 2"/>
				<Item Name="IMAQ Point" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Point"/>
				<Item Name="IMAQ ReadFile 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ ReadFile 2"/>
				<Item Name="IMAQ Rectangle" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Rectangle"/>
				<Item Name="IMAQ SetImageSize" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ SetImageSize"/>
				<Item Name="IMAQ Write BMP File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write BMP File 2"/>
				<Item Name="IMAQ Write File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write File 2"/>
				<Item Name="IMAQ Write Image And Vision Info File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write Image And Vision Info File 2"/>
				<Item Name="IMAQ Write JPEG File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write JPEG File 2"/>
				<Item Name="IMAQ Write JPEG2000 File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write JPEG2000 File 2"/>
				<Item Name="IMAQ Write PNG File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write PNG File 2"/>
				<Item Name="IMAQ Write TIFF File 2" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ Write TIFF File 2"/>
				<Item Name="NI_Vision_Development_Module.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/NI_Vision_Development_Module.lvlib"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="Overlay Results (Blob).vi" Type="VI" URL="/&lt;vilib&gt;/vision/Blob.llb/Overlay Results (Blob).vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="Particle Parameters" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Particle Parameters"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="ROI Descriptor" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/ROI Descriptor"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="nivision.dll" Type="Document" URL="nivision.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivissvc.dll" Type="Document" URL="nivissvc.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Accessory VIs" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{8E6035F8-EC0D-4291-9B04-2B8657074B01}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Accessory VIs</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../VBAI2018/Target Configuration Files/User VIs/Accessory VIs.llb</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{90B18562-BF06-4210-AF26-AFB9860AFBC1}</Property>
				<Property Name="Bld_version.build" Type="Int">371</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../VBAI2018/Target Configuration Files/User VIs/Accessory VIs.llb</Property>
				<Property Name="Destination[0].type" Type="Str">LLB</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../VBAI2018/Target Configuration Files/User VIs/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{723BA906-CE85-4F71-857C-D21A8B2C5271}</Property>
				<Property Name="Source[0].newName" Type="Str">MUG_</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/VBAI/VBAI_ExtractColorPlanes.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/!InspectionInterface.vi</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/VBAI/VBAI_Determine Carton Number.vi</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/VBAI/VBAI_UpdateResultArray.vi</Property>
				<Property Name="Source[12].type" Type="Str">VI</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/Geometry/CalPosition.vi</Property>
				<Property Name="Source[13].type" Type="Str">VI</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/VBAI/VBAI_ProcessROIs.vi</Property>
				<Property Name="Source[14].type" Type="Str">VI</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/controls/Defect Data.ctl</Property>
				<Property Name="Source[15].type" Type="Str">VI</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref">/My Computer/controls/Defect Class.ctl</Property>
				<Property Name="Source[16].type" Type="Str">VI</Property>
				<Property Name="Source[17].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[17].itemID" Type="Ref">/My Computer/VBAI/VBAI_updateDefectCount.vi</Property>
				<Property Name="Source[17].type" Type="Str">VI</Property>
				<Property Name="Source[18].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[18].itemID" Type="Ref">/My Computer/VBAI/VBAI_DefectPLCOutput.vi</Property>
				<Property Name="Source[18].type" Type="Str">VI</Property>
				<Property Name="Source[19].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[19].itemID" Type="Ref">/My Computer/subVIs/IMAQ_MultithresholdWrapper.vi</Property>
				<Property Name="Source[19].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/subVIs/Get ASCII string.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[20].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[20].itemID" Type="Ref">/My Computer/VBAI/VBAI_AngleTranslate.vi</Property>
				<Property Name="Source[20].type" Type="Str">VI</Property>
				<Property Name="Source[21].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[21].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[21].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[21].itemID" Type="Ref">/My Computer/VBAI</Property>
				<Property Name="Source[21].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[21].type" Type="Str">Container</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/VBAI/VBAI_StripPath.vi</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/VBAI/VBAI_GenerateTimeStamp.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/VBAI/VBAI_thresholdAndExtract.vi</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Utilities/ManageColorThresholdXML.vi</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/controls/Color Threshold.ctl</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/VBAI/VBAI_ClosestCarton.vi</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/VBAI/VBAI_ComputeColorResults.vi</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">22</Property>
			</Item>
			<Item Name="Geometry" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{0A228233-0E7D-4F6B-B972-F19250D272FB}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Geometry</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/Public/Documents/shared/builds/NI_AB_PROJECTNAME/Geometry.llb</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{D42D45F9-E8D2-4FB9-B66D-83E84E3FFB2F}</Property>
				<Property Name="Bld_version.build" Type="Int">265</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/Public/Documents/shared/builds/NI_AB_PROJECTNAME/Geometry.llb</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].type" Type="Str">LLB</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].Container.applyPrefix" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{9B650B33-5571-4A72-83CF-B6EFFFA9CED3}</Property>
				<Property Name="Source[0].newName" Type="Str">MUG_</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/VBAI/VBAI_ExtractColorPlanes.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/!InspectionInterface.vi</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/VBAI/VBAI_Determine Carton Number.vi</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/VBAI/VBAI_UpdateResultArray.vi</Property>
				<Property Name="Source[12].type" Type="Str">VI</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/Geometry/CalPosition.vi</Property>
				<Property Name="Source[13].type" Type="Str">VI</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/VBAI/VBAI_ProcessROIs.vi</Property>
				<Property Name="Source[14].type" Type="Str">VI</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/controls/Defect Data.ctl</Property>
				<Property Name="Source[15].type" Type="Str">VI</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref">/My Computer/controls/Defect Class.ctl</Property>
				<Property Name="Source[16].type" Type="Str">VI</Property>
				<Property Name="Source[17].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[17].itemID" Type="Ref">/My Computer/VBAI/VBAI_updateDefectCount.vi</Property>
				<Property Name="Source[17].type" Type="Str">VI</Property>
				<Property Name="Source[18].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[18].itemID" Type="Ref">/My Computer/VBAI/VBAI_DefectPLCOutput.vi</Property>
				<Property Name="Source[18].type" Type="Str">VI</Property>
				<Property Name="Source[19].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[19].itemID" Type="Ref">/My Computer/subVIs/IMAQ_MultithresholdWrapper.vi</Property>
				<Property Name="Source[19].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/subVIs/Get ASCII string.vi</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[20].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[20].itemID" Type="Ref">/My Computer/VBAI/VBAI_AngleTranslate.vi</Property>
				<Property Name="Source[20].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[20].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/VBAI/VBAI_StripPath.vi</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/VBAI/VBAI_GenerateTimeStamp.vi</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/VBAI/VBAI_thresholdAndExtract.vi</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Utilities/ManageColorThresholdXML.vi</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/controls/Color Threshold.ctl</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/VBAI/VBAI_ClosestCarton.vi</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/VBAI/VBAI_ComputeColorResults.vi</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">21</Property>
			</Item>
		</Item>
	</Item>
</Project>
