﻿$installDir = (Get-ItemProperty "hklm:\software\National Instruments\Vision Builder AI\18.0.0").Path;

$programSource = "ACS CartonScan"
$eventParams = @{
    logname = 'Application'
    source = $programSource
    eventID = 010
    category = 1
    }

if ([System.Diagnostics.EventLog]::SourceExists($programSource) -eq $false) {
	[System.Diagnostics.EventLog]::CreateEventSource($programSource, "Application")
}

$cdis = $installDir + "Vision Builder.exe"

if (!(Get-Process "Vision Builder" -ErrorAction SilentlyContinue)) {
	& $cdis
    $message = 'Restarted Vision Builder'
	write-eventlog @eventParams -entrytype information -message $message
    Start-sleep 5
    }
else{
    $message = 'VBAI already running, aborting'   
}
write-output $message